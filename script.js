const productForm = document.getElementById('product-form');
const productNameInput = document.getElementById('product-name');
const productPriceInput = document.getElementById('product-price');
const productList = document.getElementById('product-list');

productForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const productName = productNameInput.value;
    const productPrice = productPriceInput.value;

    if (productName && productPrice) {
        addProduct(productName, productPrice);
        productNameInput.value = '';
        productPriceInput.value = '';
    }
});

function addProduct(name, price) {
    // Aquí puedes realizar una solicitud POST al servidor Node.js para agregar el producto
    // Por simplicidad, solo agregamos el producto a la lista en el lado del cliente
    const productItem = document.createElement('div');
    productItem.className = 'product-item';
    productItem.innerHTML = `<p>${name}: $${price}</p><button class="delete-button">Delete</button>`;

    productList.appendChild(productItem);

    const deleteButton = productItem.querySelector('.delete-button');
    deleteButton.addEventListener('click', () => {
        productList.removeChild(productItem);
        // Aquí puedes realizar una solicitud DELETE al servidor Node.js para eliminar el producto
    });
}
