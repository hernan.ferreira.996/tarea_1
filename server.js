const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

const products = [];

app.get('/products', (req, res) => {
    // Aquí puedes devolver la lista de productos como una respuesta JSON
    res.json(products);
});

app.post('/products', (req, res) => {
    const { name, price } = req.body;
    if (name && price) {
        const newProduct = { name, price };
        products.push(newProduct);
        // Puedes agregar el producto a tu base de datos o almacenamiento aquí
        res.sendStatus(201); // Created
    } else {
        res.sendStatus(400); // Bad Request
    }
});

// Agrega rutas y lógica para las operaciones de actualización y eliminación según tus necesidades

app.listen(port, () => {
    console.log(`Server is running on port ${3000}`);
});

